<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    protected $objUser;
    protected $objOrder;

    public function __construct()
    {
        $this->objUser = new User;
        $this->objOrder = new Order;
    }

    public function index()
    {
        return view('home', [
            "users" => $this->objUser->all(), 
            "orders" => $this->objOrder->all(),
            "is_authorized" => Auth::check()
        ]);
    }
}
