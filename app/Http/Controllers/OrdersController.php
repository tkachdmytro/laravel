<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller
{
    protected $objOrder;

    public function __construct()
    {
        $this->objOrder = new Order;
    }

    public function index()
    {
        return Auth::check() ? view("order/list", ["orders" => $this->objOrder->all()]) : abort(404);
    }

    public function create()
    {
        return Auth::check() ? view('order/add', ['user_id' => Auth::user()->id]) : abort(404);
    }

    public function edit($id)
    {
        if (Auth::check()) {
            $user = Auth::user();
            $item = $this->objOrder->findOrFail($id);
            if ($item && $item->user_id == $user->id) {
                return view("order/edit", ["data" => $item, "user" => $user]);
            }
        }

        abort(404);
    }

    public function detail($id)
    {
        if (Auth::check()) {
            $user = Auth::user();
            $item = $this->objOrder->findOrFail($id);
            if ($item && $item->user_id == $user->id) {
                return view("order/detail", ["data" => $item, "user" => $user]);
            }
        }

        abort(404);
    }

    public function add(Request $request)
    {
        $res = $request->post();

        if (isset($res["user_id"]) && isset($res["order_name"])) {
            $this->objOrder->order_name = $res["order_name"];
            $this->objOrder->user_id = $res["user_id"];

            $this->objOrder->save();

            if (isset($this->objOrder->id)) {
                return response()->json([
                    'success' => true
                ]);
            }
        }

        return response()->json([
            'success' => false
        ]);
    }

    public function update(Request $request)
    {
        $res = $request->post();

        if ($res['order_id'] && isset($res["order_name"])) {
            $item = $this->objOrder->findOrFail((int) $res["order_id"]);
            if ($item) {
                $item->order_name = $res["order_name"];
                $userRes = $item->save();

                return response()->json([
                    'success' => $userRes,
                    'data' => $item
                ]);
            }
        }

        return response()->json([
            'success' => false
        ]);
    }

    public function delete(Request $request)
    {
        $res = $request->post();

        if ($res['id']) {
            $userRes = $this->objOrder->findOrFail((int) $res["id"])->delete();

            return response()->json([
                'success' => $userRes
            ]);
        }

        return response()->json([
            'success' => false
        ]);
    }
}
