<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    protected $objUser;

    public function __construct()
    {
        $this->objUser = new User;
    }

    public function index()
    {
        return Auth::check() ? view("user/list", ["users" => $this->objUser->all()]) : abort(404);
    }

    public function create()
    {
        return Auth::check() ? view('user/add') : abort(404);
    }

    public function add(Request $request)
    {
        $res = $request->post();

        if (isset($res["name"]) && isset($res["last_name"]) && isset($res["email"])) {
            $this->objUser->name = $res["name"];
            $this->objUser->last_name = $res["last_name"];
            $this->objUser->email = $res["email"];
            $this->objUser->password = "111";
            $this->objUser->save();

            if (isset($this->objUser->id)) {
                return response()->json([
                    'success' => true
                ]);
            }
        }

        return response()->json([
            'success' => false
        ]);
    }

    public function update(Request $request)
    {
        $res = $request->post();

        if ($res['user_id'] && isset($res["name"]) && isset($res["email"])) {
            $item = $this->objUser->findOrFail((int) $res["user_id"]);
            if ($item) {
                $item->name = $res["name"];
                $item->email = $res["email"];

                $userRes = $item->save();

                return response()->json([
                    'success' => $userRes
                ]);
            }
        }

        return response()->json([
            'success' => false
        ]);
    }

    public function delete(Request $request)
    {
        $res = $request->post();

        if ($res['id']) {
            $userRes = $this->objUser->find($res['id'])->delete();

            return response()->json([
                'success' => $userRes
            ]);
        }

        return response()->json([
            'success' => false
        ]);
    }

    public function edit($id)
    {
        if (Auth::check()) {
            $user = Auth::user();
            $item = $this->objUser->findOrFail($id);
            if ($item) {
                return view("user/edit", ["data" => $item]);
            }
        }

        abort(404);
    }
}
