@extends('layouts.app')

@section('content')
    <h2>User Table</h2>
    @if ($is_authorized)
        <div>
            <label>User create:</label>
            <a href="/user/add/" class="btn btn-outline-success">go to</a>
        </div>
    @endif
    <br />
    <table class="table" id="user_table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Last</th>
                <th scope="col">email</th>
                @if ($is_authorized)
                    <th scope="col">edit</th>
                @endif
                <th scope="col">delete</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $key => $user)
                <tr>
                    <th scope="row">{{ $key+1 }}</th>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->last_name }}</td>
                    <td>{{ $user->email }}</td>
                    @if ($is_authorized)
                        <td>
                            <a href="/user/edit/{{ $user->id }}/" class="btn btn-outline-success">Edit</a>
                        </td>
                    @endif
                    <td>
                        <button 
                            data-id="{{ $user->id }}" 
                            type="button" 
                            class="btn btn-outline-danger delete-js"
                        >
                            X
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <br />
    <h2>Order Table</h2>
    @if ($is_authorized)
        <div>
            <label>Order create:</label>
            <a href="/order/add/" class="btn btn-outline-success">go to</a>
        </div>
    @endif
    <br />
    <table class="table" id="order_table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">ID</th>
                <th scope="col">User ID</th>
                <th scope="col">Order Name</th>
                @if ($is_authorized)
                    <th scope="col">Edit</th>
                @endif
                <th scope="col">delete</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($orders as $key => $order)
                <tr>
                    <th scope="row">{{ $key+1 }}</th>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->user_id }}</td>
                    <td>{{ $order->order_name }}</td>
                    @if ($is_authorized)
                        <td>
                            <a href="/order/edit/{{ $order->id }}/" class="btn btn-outline-success">Edit</a>
                        </td>
                    @endif
                    <td>
                        <button 
                            data-id="{{ $order->id }}" 
                            type="button" 
                            class="btn btn-outline-danger delete-order-js"
                        >
                            X
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
