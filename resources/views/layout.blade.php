@include("include/header")
@include("include/head")
@include("include/footer")
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @yield("HEAD")
    </head>
    <body>
        <header>
            @yield("HEADER")
        </header>
        <div style="min-height: 750px; margin: 35px 0px">
            @yield("CONTENT")
        </div>
        <footer>
            @yield("FOOTER")
        </footer>
    </body>
</html>
