@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Detail order</div>

                    <div class="card-body">
                        <form method="POST" class="form-js">
                            <input type="hidden" name="order_id" value="{{ $data->id }}">
                            @csrf

                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">Order Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('order_name') is-invalid @enderror" name="order_name" value="{{ $data->order_name }}" required autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <a href="/" class="btn btn-outline-success">home</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
