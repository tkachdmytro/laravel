@extends('layouts.app')

@section('content')
    <h2>Order Table</h2>
    <div>
        <label>Order create:</label>
        <a href="/order/add/" class="btn btn-outline-success">go to</a>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">ID</th>
                <th scope="col">User ID</th>
                <th scope="col">Order Name</th>
                <th scope="col">Edit</th>
                <th scope="col">delete</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($orders as $key => $order)
                <tr>
                    <th scope="row">{{ $key+1 }}</th>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->user_id }}</td>
                    <td>{{ $order->order_name }}</td>
                    <td>
                        <a href="/order/edit/{{ $order->id }}/" class="btn btn-outline-success">Edit</a>
                    </td>
                    <td>
                        <button 
                            data-id="{{ $order->id }}" 
                            type="button" 
                            class="btn btn-outline-danger delete-order-js"
                        >
                            X
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <a href="/" class="btn btn-outline-success">Home</a>
@endsection
