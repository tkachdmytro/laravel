@extends('layouts.app')

@section('content')
<h2>User Table</h2>
    <div>
        <label>User create:</label>
        <a href="/user/add/" class="btn btn-outline-success">go to</a>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Last</th>
                <th scope="col">email</th>
                <th scope="col">edit</th>
                <th scope="col">delete</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $key => $user)
                <tr>
                    <th scope="row">{{ $key+1 }}</th>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->last_name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        <a href="/user/edit/{{ $user->id }}/" class="btn btn-outline-success">Edit</a>
                    </td>
                    <td>
                        <button 
                            data-id="{{ $user->id }}" 
                            type="button" 
                            class="btn btn-outline-danger delete-js"
                        >
                            X
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <a href="/" class="btn btn-outline-success">Home</a>
@endsection
