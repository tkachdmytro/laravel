<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/user/add', 'App\Http\Controllers\UsersController@add')->name('user-add');
Route::post('/user/update', 'App\Http\Controllers\UsersController@update')->name('user-update');
Route::post('/user/delete/{id}', 'App\Http\Controllers\UsersController@delete')->name('user-delete');

Route::post('/order/add', 'App\Http\Controllers\OrdersController@add')->name('order-add');
Route::post('/order/update', 'App\Http\Controllers\OrdersController@update')->name('order-update');
Route::post('/order/delete', 'App\Http\Controllers\OrdersController@delete')->name('order-delete');
Route::post('/order/delete/{id}', 'App\Http\Controllers\OrdersController@delete')->name('order-delete');