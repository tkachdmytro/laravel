<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', 'App\Http\Controllers\HomeController@index');

Route::get('/user/', 'App\Http\Controllers\UsersController@index');
Route::get('/user/add/', 'App\Http\Controllers\UsersController@create');
Route::get('/user/edit/{id}', 'App\Http\Controllers\UsersController@edit');
Route::get('/user/detail/{id}', 'App\Http\Controllers\UsersController@edit');

Route::get('/order/', 'App\Http\Controllers\OrdersController@index');
Route::get('/order/add/', 'App\Http\Controllers\OrdersController@create');
Route::get('/order/edit/{id}', 'App\Http\Controllers\OrdersController@edit');
Route::get('/order/detail/{id}/', 'App\Http\Controllers\OrdersController@detail');
